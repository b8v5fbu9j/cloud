# 浏览器调试

##### 错误收集示例,实时监控网页应用的错误。

###### Jquery 示例

```javascript
<script type="text/javascript">
    window.onerror = function(message, source, lineno, colno, error) {
        const err = error.stack.split("\n");
        $.ajax({
            type: "POST",
            url: "https://baas.qingful.com/2.0/debug/add",
            data: {
                name: error.message, //名称
                type: err[0].split(":")[0], //类型
                message: message, //信息
                info: error.stack, //详情
                device: navigator.appName, //设备
                system: navigator.appVersion, //系统
                remark: `浏览器名称：${navigator.appName},
                     浏览器版本：${navigator.appVersion},
                     显示器高度：${window.screen.height},
                     显示器宽度：${window.screen.width},
                     错误信息：${message},
                     出错文件：${source},
                     出错行号：${lineno},
                     出错列号：${colno},
                     错误详情：${error.stack}` //备注
            },
            dataType: "json",
            headers: {
                'Content-Type': 'application/json',
                'x-qingful-appid': appId,
                'x-qingful-appkey': appKey
            },
            success: function(data) {
                console.log(data);
            }
        });
    }
</script>
 ```

###### Vue 示例

Vue.js 2.2.0+提供了 errorHandler。

```javascript
const axios = require("axios");

function formatComponentName(vm){
  if (vm.$root === vm) return 'root';

  var name = vm._isVue ? (vm.$options && vm.$options.name) || (vm.$options && vm.$options._componentTag) : vm.name;
  return (name ? 'component <' + name + '>' : 'anonymous component') + (vm._isVue && vm.$options && vm.$options.__file ? ' at ' + (vm.$options && vm.$options.__file) : '');
}

Vue.config.errorHandler = function (err, vm, info) {
  // handle error
  // `info` 是 Vue 特定的错误信息，比如错误所在的生命周期钩子
  // 只在 2.2.0+ 可用
  const componentName = formatComponentName(vm);
  const propsData = vm.$options && vm.$options.propsData;
  const error = err.stack.split("\n");
  const data = {
      name: err.message, //名称
      type: error[0].split(":")[0], //类型
      message: err.message, //信息
      info: err.stack, //详情
      device: navigator.appName, //设备
      system: navigator.appVersion, //系统
      remark: `浏览器名称：${navigator.appName},
           浏览器版本：${navigator.appVersion},
           显示器高度：${window.screen.height},
           显示器宽度：${window.screen.width},
           出错文件：${componentName},
           propsData：${propsData},
           错误信息：${err.message},
           错误详情：${err.stack}` //备注
  }
  axios({
      method: 'post',
      url: 'https://baas.qingful.com/2.0/debug/add',
      data: data,
      headers: {
        'Content-Type': 'application/json',
        'x-qingful-appid': appId,
        'x-qingful-appkey': appKey
      }
  }).then(function(res) {
      console.log(res);
  });
}
```

